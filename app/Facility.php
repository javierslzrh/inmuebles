<?php namespace App;

class Facility extends Base
{
    protected $primaryKey = 'id';
    protected $table = 'facilities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];
}
