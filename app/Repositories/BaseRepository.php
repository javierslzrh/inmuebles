<?php namespace App\Repositories;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Container\Container as App;
use Illuminate\Support\Collection;


class BaseRepository extends Repository {

    protected $model;

    public function __construct($model, App $app, Collection $collection){
        $this->model = $model;
        parent::__construct($app, $collection);
    }

    public function model() {
        return $this->model;
    }
}