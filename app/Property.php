<?php namespace App;

class Property extends Base
{
    protected $primaryKey = 'id';
    protected $table = 'properties';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'address', 'town', 'county', 'country', 'state_id'
    ];

    public function facilities(){
        return $this->belongsToMany('App\Facility', 'properties_facilities', 'property_id', 'facility_id');
    }

    public function state(){
        return $this->belongsTo('App\State', 'state_id');
    }
}
