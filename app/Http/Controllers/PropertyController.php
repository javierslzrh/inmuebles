<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\View;

class PropertyController extends Controller {

    private $property;
    private $state;
    private $facilities;
    private $propertiesFacilities;

    public function __construct()
    {
        $this->middleware('auth');
        $this->property = new \App\Repositories\BaseRepository('App\Property', app(), \Illuminate\Support\Collection::make());
        $this->state = new \App\Repositories\BaseRepository('App\State', app(), \Illuminate\Support\Collection::make());
        $this->facilities = new \App\Repositories\BaseRepository('App\Facility', app(), \Illuminate\Support\Collection::make());
        $this->propertiesFacilities = new \App\Repositories\BaseRepository('App\PropertiesFacilities', app(), \Illuminate\Support\Collection::make());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $properties = $this->property->all();
        return View::make('property.index', [
            'properties' => $properties
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $states = $this->state->all();
        $facilities = $this->facilities->all();

        return View::make('property.create', [
            'edit' => false,
            'states' => $states,
            'facilities' => $facilities
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'address' => 'required',
            'town' => 'required',
            'county' => 'required',
            'country' => 'required',
            'state_id' => 'required',
            'facilities' => 'required'
        ]);

        $property = $this->property->create($request->all());
        foreach($request->facilities as $facilityId){
            $this->propertiesFacilities->create([
                'property_id' => $property->id,
                'facility_id' => $facilityId
            ]);

        }

        return back()->with([
            'status' => 'success',
            'message' => 'Property registered!'
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $property = $this->property->find($id);

        return View::make('property.show', [
            'property' => $property
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $property = $this->property->find($id);
        $states = $this->state->all();
        $facilities = $this->facilities->all();

        return View::make('property.create', [
            'edit' => true,
            'property' => $property,
            'states' => $states,
            'facilities' => $facilities
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'address' => 'required',
            'town' => 'required',
            'county' => 'required',
            'country' => 'required',
            'state_id' => 'required',
            'facilities' => 'required'
        ]);

        $this->property->update([
            'title' => $request->title,
            'address' => $request->address,
            'town' => $request->town,
            'county' => $request->county,
            'country' => $request->country,
            'state_id' => $request->state_id,
        ], $id);

        $property = $this->property->find($id);
        foreach($property->facilities as $facility){
            if(array_search($facility->id, $request->facilities) === false){
                $propertyFacility = $this->propertiesFacilities->findWhere([
                    'property_id' => $property->id,
                    'facility_id' => $facility->id
                ])->get(0);
                $this->propertiesFacilities->delete($propertyFacility->id);
            }
        }

        foreach($request->facilities as $facilityId){
            $propertyFacility = $this->propertiesFacilities->findWhere([
                'property_id' => $property->id,
                'facility_id' => $facilityId
            ])->get(0);

            if(!isset($propertyFacility->id)){
                $this->propertiesFacilities->create([
                    'property_id' => $property->id,
                    'facility_id' => $facilityId
                ]);
            }
        }

        return back()->with([
            'status' => 'success',
            'message' => 'Property updated!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $propertiesFacilities = $this->propertiesFacilities->findWhere([
            'property_id' => $id
        ]);

        foreach($propertiesFacilities as $propertiesFacility){
            $this->propertiesFacilities->delete($propertiesFacility->id);
        }

        $this->property->delete($id);

        return back()->with([
            'status' => 'success',
            'message' => 'Property deleted!'
        ]);
    }
}
