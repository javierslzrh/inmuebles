<?php namespace App;

class PropertiesFacilities extends Base
{
    protected $primaryKey = 'id';
    protected $table = 'properties_facilities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'property_id', 'facility_id'
    ];
}
