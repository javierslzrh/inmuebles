<?php namespace App;

class State extends Base
{
    protected $primaryKey = 'id';
    protected $table = 'states';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];
}
