<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Javier Salazar',
            'email' => 'javierslzrh@gmail.com',
            'password' => bcrypt('12345'),
        ]);
    }
}
