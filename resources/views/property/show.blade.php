@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Control panel
                        <div class="pull-right">
                            <a class="btn btn-default" href="{{ url('/') }}"><i class="fa fa-btn  fa-chevron-left"></i> Go back</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="panel-body">
                        <table class="table">
                                <tr>
                                    <th>ID</th><td>{{ $property->id }}</td>
                                </tr>
                                <tr>
                                    <th>Title</th><td>{{ $property->title }}</td>
                                </tr>
                                <tr>
                                    <th>Description</th><td>{{ $property->description }}</td>
                                </tr>
                                <tr>
                                    <th>Address</th><td>{{ $property->address }}</td>
                                </tr>
                                <tr>
                                    <th>Town</th><td>{{ $property->town }}</td>
                                </tr>
                                <tr>
                                    <th>County</th><td>{{ $property->county }}</td>
                                </tr>
                                <tr>
                                    <th>Country</th><td>{{ $property->country }}</td>
                                </tr>
                                <tr>
                                    <th>State</th><td>{{ $property->state->name }}</td>
                                </tr>
                                <tr>
                                    <th>Facilities</th>
                                    <td>
                                        @foreach($property->facilities as $facility)
                                            <p>{{$facility->name}}</p>
                                        @endforeach
                                    </td>
                                </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection