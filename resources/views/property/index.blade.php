@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Control panel
                        <div class="pull-right">
                            <a class="btn btn-default" href="{{ url('/property/create') }}"><i class="fa fa-btn fa-floppy-o"></i> Add new</a>

                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="panel-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>State</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($properties as $property)
                                <tr>
                                    <td>{{ $property->id }}</td>
                                    <td>{{ $property->title }}</td>
                                    <td>{{ $property->state->name }}</td>
                                    <td>
                                        <form name="delete" method="post" action="{{ url('/property/'.$property->id) }}">
                                            <input name="_method" type="hidden" value="DELETE" />
                                            <div class="btn-group">
                                                <a href="{{ url('/property/'.$property->id)  }}" class="btn btn-sm btn-default" title="View Details" >
                                                    <i class="fa fa-eye"></i>
                                                </a>

                                                <a href="{{ url('/property/'.$property->id.'/edit') }}" class="btn btn-sm btn-default" title="Edit" >
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                {{ csrf_field() }}
                                                <button type="submit" href="{{ url('/property/'.$property->id.'/edit') }}" class="btn btn-sm btn-danger delete" title="Delete" >
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection