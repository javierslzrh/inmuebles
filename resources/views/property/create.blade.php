@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @if($edit != true) Register property @else Edit property @endif
                            <div class="pull-right">
                                <a class="btn btn-default" href="{{ url('/') }}"><i class="fa fa-btn  fa-chevron-left"></i> Go back</a>
                            </div>
                            <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">

                        <form class="form-horizontal" role="form"
                              @if($edit != true)
                                method="POST" action="{{ url('/property') }}"
                              @else
                                method="POST" action="{{ url('/property/'.$property->id) }}"
                              @endif
                        >
                            @if($edit)
                                <input name="_method" type="hidden" value="PUT" />
                                <input type="hidden" name="id" value="{{ isset($property) ? $property->id : old('id') }}">
                            @endif
                            {{ csrf_field() }}
                                @if (\Session::has('status') && \Session::get('status') == 'success')
                                <div class="alert alert-success"><i class="fa fa-check" aria-hidden="true"></i>{{ \Session::get('message') }}</div>
                            @endif
                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="title" class="col-md-4 control-label">Title</label>

                                <div class="col-md-6">
                                    <input id="title" type="text" class="form-control" name="title" value="{{ isset($property) ? $property->title : old('title') }}" required="required" />

                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="description" class="col-md-4 control-label">Description</label>

                                <div class="col-md-6">
                                    <textarea id="description" class="form-control" name="description">{{ isset($property) ? $property->description : old('description') }}</textarea>

                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="address" class="col-md-4 control-label">Address</label>

                                <div class="col-md-6">
                                    <textarea id="address" class="form-control" name="address" required="required">{{ isset($property) ? $property->address : old('address') }}</textarea>

                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('town') ? ' has-error' : '' }}">
                                <label for="town" class="col-md-4 control-label">Town</label>

                                <div class="col-md-6">
                                    <input id="town" type="text" class="form-control" name="town" value="{{ isset($property) ? $property->town : old('town') }}" required="required" />

                                    @if ($errors->has('town'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('town') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('county') ? ' has-error' : '' }}">
                                <label for="county" class="col-md-4 control-label">County</label>

                                <div class="col-md-6">
                                    <input id="county" type="text" class="form-control" name="county" value="{{ isset($property) ? $property->county : old('county') }}" required="required" />

                                    @if ($errors->has('county'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('county') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                                <label for="country" class="col-md-4 control-label">Country</label>

                                <div class="col-md-6">
                                    <input id="country" type="text" class="form-control" name="country" value="{{ isset($property) ? $property->country : old('country') }}" />

                                    @if ($errors->has('country'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('facilities') ? ' has-error' : '' }}">
                                <label for="facilities" class="col-md-4 control-label">Facilities</label>

                                <div class="col-md-6">
                                    <select id="facilities" name="facilities[]" class="form-control" multiple="multiple" required="required">
                                        @foreach ($facilities as $facility)
                                                <option value="{{ $facility->id }}"
                                                @if(isset($property))
                                                    @foreach($property->facilities as $itemFacility)
                                                        @if($facility->id == $itemFacility->id)
                                                            {{ 'selected = "selected"' }}
                                                        @endif
                                                    @endforeach
                                                @elseif($facility->id == old('facility'))
                                                    {{ 'selected = "selected"' }}
                                                @endif
                                                > {{ $facility->name  }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('facilities'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('facilities') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                                <label for="state_id" class="col-md-4 control-label">State</label>
                                <div class="col-md-6">
                                    <select id="state_id" name="state_id" class="form-control" required="required">
                                        @foreach ($states as $state)
                                            <option value="{{ $state->id }}"
                                                @if($state->id == old('state'))
                                                    {{ 'selected = "selected"' }}
                                                @endif > {{ $state->name  }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('state'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('state') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-6">
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-btn fa-floppy-o"></i>
                                        @if($edit != true)
                                             Register
                                        @else
                                             Update
                                        @endif
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection