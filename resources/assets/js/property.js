$(document).ready(function() {
    $('select[name="facilities[]"]').select2({
        allowClear: true
    });

    $('.delete').on('click', function(ev){
        ev.preventDefault();
        var element = $(this);
        var prompt = $.prompt("Are you sure?.", {
            title: "Delete property",
            buttons: { "Yes": true, "No": false },
            submit: function(e,v,m,f){
                // use e.preventDefault() to prevent closing when needed or return false.
                // e.preventDefault();
                if(v){
                    $(element).parents('form').first().submit();
                }
            }
        });

    });
});
